use input::Input;
use parser::{Parser, Error};

pub struct Map<P, F> {p: P, f: F}

impl<'a, P, F> Map< P, F> {
    pub fn new(p: P, f: F) -> Self {
        Map {p, f}
    }
}

impl<'a, O1, O2, P, F> Parser<'a> for Map<P, F>
where
    P: Parser<'a, Output = O1>,
    F: Fn(O1) -> O2,
    Self: Sized
{
    type Output = O2;
    fn parse(&self, input: Input<'a>) -> Result<(O2, Input<'a>), Error>
    {
        let f = &self.f;
        let (o1, r) = self.p.parse(input)?;
        let o2 = f(o1);

        Ok((o2, r))
    }
}

