use input::Input;
use parser::{Parser, Error};

pub struct And<P1, P2> {a: P1, b: P2}

impl<'a, P1, P2> And<P1, P2> {
    pub fn new(a: P1, b: P2) -> Self {
        And {a, b}
    }
}

impl<'a, O1, O2, P1, P2> Parser<'a> for And<P1, P2>
where
    P1: Parser<'a, Output = O1>,
    P2: Parser<'a, Output = O2>,
    Self: Sized
{
    type Output = (O1, O2);
    fn parse(&self, input: Input<'a>) -> Result<(Self::Output, Input<'a>), Error>
    {
        let (o1, r1) = self.a.parse(input)?;
        let (o2, r2) = self.b.parse(r1)?;

        Ok(((o1, o2), r2))

    }
}
