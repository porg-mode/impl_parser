use input::Input;
use parser::{Parser, Error};

pub struct Or<P1, P2> {a: P1, b: P2}

impl<'a, P1, P2> Or<P1, P2> {
    pub fn new(a: P1, b: P2) -> Self {
        Or {a, b}
    }
}

impl<'a, O, P1, P2> Parser<'a> for Or<P1, P2>
where
    P1: Parser<'a, Output = O>,
    P2: Parser<'a, Output = O>,
    Self: Sized
{
    type Output = O;
    fn parse(&self, input: Input<'a>) -> Result<(Self::Output, Input<'a>), Error>
    {
        match self.a.parse(input.clone()) {
            Ok((o1, r1)) => Ok((o1, r1)),
            Err(_) => self.b.parse(input)
        }
    }
}
