use input::Input;
use parser::Parser;
use combinators::concrete::{Map, And, Or};

pub fn unbox<'a, O, P>(parser: Box<P>) -> impl Parser<'a, Output = O>
where P: Parser<'a, Output = O> {
    move |input: Input<'a>| {
        parser.parse(input)
    }
}

pub fn dummy_parser<'a, O>() -> impl Parser<'a, Output = O> where O : Default {
    move |input: Input<'a>| {
        Ok((O::default(), input))
    }
}


pub fn char_where<'a>(f: impl Fn(char) -> bool) -> impl Parser<'a, Output = char> {
    move |input: Input<'a>| {
        let first = input.slice().chars().nth(0);
        match first {
            Some(letter) if f(letter) => {
                let (_, rest) = input.consume(1)?;
                Ok((letter, rest))
            },
            Some(letter) => Err(format!("Expected `*` found `{}`", letter)),
            None => Err(format!("Expected `*` found end of file"))

        }
    }
}

pub fn single_char<'a>(letter: char) -> impl Parser<'a, Output = char> {
    char_where(move |c| c == letter)
}

pub fn any_char<'a>() -> impl Parser<'a, Output = char> {
    move |input: Input<'a>| {
        let (parsed, rest) = input.consume(1)?;
        let parsed_char = parsed.chars().nth(0).unwrap();

        Ok((parsed_char, rest))
    }
}

pub fn space<'a>() -> impl Parser<'a, Output = char> {
    char_where(move |letter| letter == ' ' || letter == '\t')
}

pub fn begin<'a>() -> impl Parser<'a, Output = ()> {
    move |input: Input<'a>| {
        if input.location() == 0 {
            return Ok(((), input));
        }
        Err(format!("Expected being in the begginning of stream"))
    }
}

pub fn eof<'a>() -> impl Parser<'a, Output = ()> {
    move |input: Input<'a>| {
        if input.eof() {
            return Ok(((), input));
        }

        let first = input.slice().chars().nth(0);
        Err(format!("Expected end of file, found `{}`", first.unwrap()))
    }
}

pub fn string<'a>(s: &'a str) -> impl Parser<'a, Output = String> {
    seq(s.chars().map(single_char).collect()).map(|v| v.iter().collect())
}

// ---

pub fn and<'a, O1, O2>(a: impl Parser<'a, Output = O1>, b: impl Parser<'a, Output = O2>) -> impl Parser<'a, Output = (O1, O2)> {
    And::new(a, b)
}

pub fn seq<'a, O>(parsers: Vec<impl Parser<'a, Output = O>>) -> impl Parser<'a, Output = Vec<O>> {
    move |input: Input<'a>| {
        let mut next = input;
        let mut parsed : Vec<O> = vec![];
        for p in parsers.iter() {
            let (o, r) = p.parse(next)?;

            next = r;
            parsed.push(o);
        }

        Ok((parsed, next))
    }
}

pub fn or<'a, O>(a: impl Parser<'a, Output = O>, b: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = O> {
    Or::new(a, b)
}

pub fn not<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = char> {
    move |input: Input<'a>| {
        match parser.parse(input.clone()) {
            Ok(_) => Err(format!("Expected everything but `???`")),
            Err(_) => any_char().parse(input)
        }
    }
}

pub fn prev<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = O> {
    move |input: Input<'a>| {
        let p_input = input.back(1)?;
        parser.parse(p_input)
    }
}

pub fn prev_or_begin<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = Option<O>> {
    let p = prev(parser).map(Some);
    move |input: Input<'a>| {
        if input.location() == 0 {
            return Ok((None, input))
        }
        p.parse(input)
    }
}

pub fn lookup<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = O> {
    move |input: Input<'a>| {
        let (o, _) = parser.parse(input.clone())?;
        Ok((o, input))
    }
}

pub fn lookup_or_eof<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = Option<O>> {
    let p = lookup(parser).map(Some);
    move |input: Input<'a>| {
        if input.eof() {
            return Ok((None, input));
        }
        p.parse(input)
    }
}

pub fn map<'a, O1, O2>(parser: impl Parser<'a, Output = O1>, f: impl Fn(O1) -> O2) -> impl Parser<'a, Output = O2> {
    Map::new(parser, f)
}

pub fn many1<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = Vec<O>> {
    move |input: Input<'a>| {
        let (o1, r1) = parser.parse(input)?;
        let mut parsed = vec![o1];
        let mut next = r1;
        loop {
            match parser.parse(next.clone()) {
                Ok((o, r)) => {
                    parsed.push(o);
                    next = r;
                },
                Err(_) => { break; }
            };
        }

        Ok((parsed, next))
    }
}

pub fn many0<'a, O>(parser: impl Parser<'a, Output = O>) -> impl Parser<'a, Output = Vec<O>> {
    let m = many1(parser);
    move |input: Input<'a>| {
        match m.parse(input.clone()) {
            Ok(ok) => Ok(ok),
            Err(_) => Ok((vec![], input))
        }
    }
}

#[cfg(test)]
mod tests {
    use combinators::basic::*;

    #[test]
    fn dummy_repeat_test() {
        let parser = dummy_parser::<i32>();
        let input = Input::new("foo");
        parser.parse(input.clone()).unwrap();
        parser.parse(input).unwrap();
    }

    #[test]
    fn single_char_test() {
        let parser = single_char('*');

        let result = parser.parse(Input::new("***"));

        assert_eq!(result, Ok(('*', Input::from("**", 1, "***"))));
    }

    #[test]
    fn no_char_test() {
        let parser = single_char('*');
        let result = parser.parse(Input::new(""));

        assert_eq!(result, Err("Expected `*` found end of file".into()));
    }

    #[test]
    fn wrong_char_test() {
        let parser = single_char('*');
        let result = parser.parse(Input::new("a"));

        assert_eq!(result, Err("Expected `*` found `a`".into()));
    }

    #[test]
    fn and_test() {
        let parser = and(
            single_char('a'),
            single_char('b')
        );

        let result = parser.parse(Input::new("abc"));

        assert_eq!(result, Ok((('a', 'b'), Input::from("c", 2, "abc"))));
    }

    #[test]
    fn and_method_test() {
        let parser = single_char('a').and(single_char('b'));

        let result = parser.parse(Input::new("abc"));

        assert_eq!(result, Ok((('a', 'b'), Input::from("c", 2, "abc"))));
    }

    #[test]
    fn or_test() {
        let parser = or(
            single_char('a'),
            single_char('b')
        );

        let result = parser.parse(Input::new("a"));

        assert_eq!(result, Ok(('a', Input::end("a", 1))));

        let result2 = parser.parse(Input::new("b"));

        assert_eq!(result2, Ok(('b', Input::end("b", 1))));

        let result3 = parser.parse(Input::new("c"));

        assert_eq!(result3, Err("Expected `*` found `c`".into()));
    }

    #[test]
    fn or_method_test() {
        let parser = single_char('a').or(single_char('b'));

        let result = parser.parse(Input::new("a"));

        assert_eq!(result, Ok(('a', Input::end("a", 1))));

        let result2 = parser.parse(Input::new("b"));

        assert_eq!(result2, Ok(('b', Input::end("b", 1))));

        let result3 = parser.parse(Input::new("c"));

        assert_eq!(result3, Err("Expected `*` found `c`".into()));
    }

    #[test]
    fn not_test() {
        let parser = not(single_char('*'));
        let result = parser.parse(Input::new("a"));

        assert_eq!(result, Ok(('a', Input::end("a", 1))));

        let result2 = parser.parse(Input::new("*"));

        assert_eq!(result2, Err("Expected everything but `???`".into()));
    }

    #[test]
    fn not_and_test() {
        let parser = not(and(
            single_char('*'),
            single_char('*')
        ));

        let result = parser.parse(Input::new("a"));

        assert_eq!(result, Ok(('a', Input::end("a", 1))));

        let result2 = parser.parse(Input::new("*a"));

        assert_eq!(result2, Ok(('*', Input::from("a", 1, "*a"))));

        let result2 = parser.parse(Input::new("**"));

        assert_eq!(result2, Err("Expected everything but `???`".into()));
    }

    #[test]
    fn map_test() {
        let parser = map(single_char('1'), |_| 1);

        let result = parser.parse(Input::new("1"));

        assert_eq!(result, Ok((1, Input::end("1", 1))));
    }

    #[test]
    fn map_method_test() {
        let parser = single_char('1').map(|_| 1);

        let result = parser.parse(Input::new("1"));

        assert_eq!(result, Ok((1, Input::end("1", 1))));
    }

    #[test]
    fn many1_test() {
        let parser = many1(single_char('*'));

        let result = parser.parse(Input::new("***a"));

        assert_eq!(result, Ok((vec!['*', '*', '*'], Input::from("a", 3, "***a"))));

        let result2 = parser.parse(Input::new("a"));

        assert_eq!(result2, Err("Expected `*` found `a`".into()));
    }

    #[test]
    fn many1_until_eof_test() {
        let parser = many1(single_char('*'));

        let result = parser.parse(Input::new("***"));

        assert_eq!(result, Ok((vec!['*', '*', '*'], Input::from("", 3, "***"))));
    }

    #[test]
    fn many0_test() {
        let parser = many0(single_char('*'));

        let result = parser.parse(Input::new("***a"));

        assert_eq!(result, Ok((vec!['*', '*', '*'], Input::from("a", 3, "***a"))));

        let result2 = parser.parse(Input::new("a"));

        assert_eq!(result2, Ok((vec![], Input::from("a", 0, "a"))));
    }

    #[test]
    fn many0_until_eof_test() {
        let parser = many0(single_char('*'));

        let result = parser.parse(Input::new("***"));

        assert_eq!(result, Ok((vec!['*', '*', '*'], Input::from("", 3, "***"))));

        let result = parser.parse(Input::new(""));

        assert_eq!(result, Ok((vec![], Input::from("", 0, ""))));
    }

    #[test]
    fn unbox_test() {
        let boxed = single_char('f').boxed();

        let unboxed = unbox(boxed);

        let result = unboxed.parse(Input::new("f"));

        assert_eq!(result, Ok(('f', Input::from("", 1, "f"))));
    }
}
