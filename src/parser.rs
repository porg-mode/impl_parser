use input::Input;
use combinators::concrete::{And, Or, Map};

pub type Error = String;

pub trait Parser<'a> {
    type Output;
    fn parse(&self, input: Input<'a>) -> Result<(Self::Output, Input<'a>), Error>;


    fn map<O2, F>(self, f: F) -> Map<Self, F>
    where F: Fn(Self::Output) -> O2,
          Self: Sized
    {
        Map::new(self, f)
    }

    fn and<O2, P2>(self, p: P2) -> And<Self, P2>
    where P2: Parser<'a, Output = O2>,
          Self: Sized
    {
        And::new(self, p)
    }

    fn or<P2>(self, p: P2) -> Or<Self, P2>
    where P2: Parser<'a, Output = Self::Output>,
          Self: Sized
    {
        Or::new(self, p)
    }

    fn boxed(self) -> Box<Self>
    where Self: Sized {
        Box::new(self)
    }
}

impl<'a, F, O> Parser<'a> for F
where F: Fn(Input<'a>) -> Result<(O, Input<'a>), Error> {
    type Output = O;
    fn parse(&self, input: Input<'a>) -> Result<(O, Input<'a>), Error> {
        self(input)
    }
}
