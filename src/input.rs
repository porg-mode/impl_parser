#[derive(Debug, PartialEq, Clone)]
pub struct Input<'a> {
    slice: &'a str,
    location: usize,
    full_slice: &'a str
}

impl<'a> Input<'a> {
    pub fn from(input: &'a str, loc: usize, full: &'a str) -> Input<'a> {
        Input {
            slice: input,
            location: loc,
            full_slice: full
        }
    }

    pub fn new(input: &'a str) -> Input<'a> {
        Input {
            slice: input,
            location: 0,
            full_slice: input
        }
    }

    pub fn end(full: &'a str, loc: usize) -> Input<'a> {
        Input {
            slice: "",
            full_slice: full,
            location: loc
        }
    }

    pub fn prev(&self) -> &'a str {
        &self.slice[..self.location]
    }

    pub fn slice(&self) -> &'a str { self.slice }

    pub fn len(&self) -> usize { self.slice.len() }

    pub fn eof(&self) -> bool { self.slice.len() <= 0 }

    pub fn location(&self) -> usize { self.location }

    pub fn back(&self, size: usize) -> Result<Input<'a>, String> {
        if size > self.location {
            return Err(format!("To far!"));
        }

        let loc = self.location - size;

        Ok(Input {
            full_slice: self.full_slice,
            slice: &self.full_slice[loc..],
            location: loc
        })
    }

    pub fn consume(&self, size: usize) -> Result<(&'a str, Input<'a>), String> {
        if self.len() < size {
            return Err("Unexpected end of file".into());
        }

        let parsed = &self.slice[..size];
        let rest = &self.slice[size..];

        Ok((parsed, Input {
            full_slice: self.full_slice,
            slice: rest,
            location: self.location + size
        }))
    }
}
